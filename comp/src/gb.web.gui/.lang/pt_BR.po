#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.web.gui 3.18.90\n"
"PO-Revision-Date: 2023-03-02 01:58 UTC\n"
"Last-Translator: benoit <benoit@benoit-TOWER>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: Message.class:52
msgid "Error"
msgstr "Erro"

#: WebAudio.class:74
msgid "Html audio not supported"
msgstr ""

#: Message.class:34
msgid "Information"
msgstr "Informação"

# gb-ignore
#: FMessage.webform:45
msgid "OK"
msgstr ""

#: Message.class:58
msgid "Question"
msgstr "Questão"

#: Message.class:46
msgid "Warning"
msgstr "Aviso"

# gb-ignore
#: .project:1
msgid "Web application development using processes as session"
msgstr ""

